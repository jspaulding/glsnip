# glsnip - GitLab snippet command line utility

```bash
$ glsnip consumer.properties
security.protocol=SSL
ssl.protocol=TLSv1.2
ssl.truststore.password=Jell0123123
ssl.truststore.location=/tmp/server.truststore.jks
```
